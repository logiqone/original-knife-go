package model

import (
	"database/sql"
	"time"
)

type Product struct {
	ID               int64          `db:"id"`
	Freeze           bool           `db:"freeze"`
	Deleted          int8           `db:"deleted"`
	Code             string         `db:"code"`
	CodeKupinoj      sql.NullString `db:"code_kupinoj"`
	Name             string         `db:"name"`
	BrandID          int64          `db:"brand_id"`
	BrandName        string         `db:"brand_name"`
	BrandCode        string         `db:"brand_code"`
	Availability     string         `db:"availability"`
	Weight           int64          `db:"weight"`
	Price            float64        `db:"price"`
	Novelty          bool           `db:"novelty"`
	Bestseller       bool           `db:"bestseller"`
	CollectionID     sql.NullInt64  `db:"collection_id"`
	DiscountID       sql.NullInt64  `db:"discount_id"`
	ShortDescription sql.NullString `db:"short_description"`
	FullDescription  sql.NullString `db:"full_description"`
	VideoLink        sql.NullString `db:"video_link"`
	PromoTitle       sql.NullString `db:"promo_title"`
	PromoDescription sql.NullString `db:"promo_description"`
	PromoKeywords    sql.NullString `db:"promo_keywords"`
	PromoRobots      sql.NullString `db:"promo_robots"`
	Sort             int64          `db:"sort"`
	DateCreated      time.Time      `db:"date_created"`
	DateModify       time.Time      `db:"date_modify"`
}
