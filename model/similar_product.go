package model

//go:generate reform

// StoreSimilarProduct represents a row in store_similar_product table.
//reform:store_similar_product
type StoreSimilarProduct struct {
	IDProduct        int32 `reform:"id_product"`
	IDProductSimilar int32 `reform:"id_product_similar"`
}
