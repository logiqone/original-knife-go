package model

//go:generate reform

// StoreRole represents a row in store_role table.
//reform:store_role
type StoreRole struct {
	Name string `reform:"name"`
}
