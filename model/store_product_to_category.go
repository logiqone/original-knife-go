package model

//go:generate reform

// StoreProductToCategory represents a row in store_product_to_category table.
//reform:store_product_to_category
type StoreProductToCategory struct {
	ID         int32 `reform:"id,pk"`
	ProductID  int32 `reform:"product_id"`
	CategoryID int32 `reform:"category_id"`
}
