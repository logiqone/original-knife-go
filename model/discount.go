package model

type Discount struct {
	ID               int32   `reform:"id,pk"`
	Code             string  `reform:"code"`
	Name             string  `reform:"name"`
	Percentage       int32   `reform:"percentage"`
	DescriptionTitle string  `reform:"description_title"`
	Description      string  `reform:"description"`
	PromoTitle       *string `reform:"promo_title"`
	PromoDescription *string `reform:"promo_description"`
	PromoKeywords    *string `reform:"promo_keywords"`
	PromoRobots      *string `reform:"promo_robots"`
}
