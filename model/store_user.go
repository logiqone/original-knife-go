package model

//go:generate reform

// StoreUser represents a row in store_user table.
//reform:store_user
type StoreUser struct {
	ID          int32   `reform:"id,pk"`
	Username    string  `reform:"username"`
	Password    string  `reform:"password"`
	AuthKey     *string `reform:"auth_key"`
	AccessToken *string `reform:"access_token"`
	Name        *string `reform:"name"`
	Email       string  `reform:"email"`
	Phone       *string `reform:"phone"`
	Address     *string `reform:"address"`
	CookieID    *string `reform:"cookie_id"`
	Role        string  `reform:"role"`
	Discount    int32   `reform:"discount"`
}
