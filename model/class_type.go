package model

type ClassType struct {
	ID               int32    `db:"id,pk"`
	ProductID        int32    `db:"product_id"`
	Volume           *float32 `db:"volume"`
	Length           *float32 `db:"length"`
	Width            *float32 `db:"width"`
	Height           *float32 `db:"height"`
	Thickness        *float32 `db:"thickness"`
	Depth            *float32 `db:"depth"`
	Diameter         *float32 `db:"diameter"`
	Material         *string  `db:"material"`
	Packaging        *string  `db:"packaging"`
	SharpeningAngle1 *int32   `db:"sharpening_angle1"`
	SharpeningAngle2 *int32   `db:"sharpening_angle2"`
	SharpeningAngle3 *int32   `db:"sharpening_angle3"`
	SharpeningAngle4 *int32   `db:"sharpening_angle4"`
	Color            *string  `db:"color"`
	Design           *string  `db:"design"`
	Filter           *string  `db:"filter"`
	Object01         *string  `db:"object01"`
	Object02         *string  `db:"object02"`
	Object03         *string  `db:"object03"`
	Object04         *string  `db:"object04"`
	Object05         *string  `db:"object05"`
	Object06         *string  `db:"object06"`
	Object07         *string  `db:"object07"`
	Object08         *string  `db:"object08"`
	Object09         *string  `db:"object09"`
	Object10         *string  `db:"object10"`
	RemovingCap      *string  `db:"removing_cap"`
	RodType          *string  `db:"rod_type"`
	InkColor         *string  `db:"ink_color"`
	BladeLength      *float32 `db:"blade_length"`
	HandleMaterial   *string  `db:"handle_material"`
	AxMaterial       *string  `db:"ax_material"`
	WebMaterial      *string  `db:"web_material"`
	ToothPitch       *string  `db:"tooth_pitch"`
	InnerSize        *float32 `db:"inner_size"`
	ButtThickness    *float64 `db:"butt_thickness"`
}
