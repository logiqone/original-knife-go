package model

import (
	"time"
)

//go:generate reform

// StoreOrder represents a row in store_order table.
//reform:store_order
type StoreOrder struct {
	ID            int32     `reform:"id,pk"`
	ClientID      *int32    `reform:"client_id"`
	ClientName    *string   `reform:"client_name"`
	ClientPhone   string    `reform:"client_phone"`
	ClientEmail   *string   `reform:"client_email"`
	ClientAddress *string   `reform:"client_address"`
	Comment       *string   `reform:"comment"`
	StoreComment  *string   `reform:"store_comment"`
	Products      string    `reform:"products"`
	OrderDiscount *int32    `reform:"order_discount"`
	Created       time.Time `reform:"created"`
	Total         int32     `reform:"total"`
	Status        []byte    `reform:"status"` // FIXME unhandled database type "enum"
	PayType       *int8     `reform:"pay_type"`
	TransactionID *string   `reform:"transaction_id"`
}
