package model

import (
	"database/sql"
)

type Collection struct {
	ID               int64          `db:"id"`
	Code             string         `db:"code"`
	Name             string         `db:"name"`
	BrandID          int64          `db:"brand_id"`
	DescriptionTitle string         `db:"description_title"`
	Description      string         `db:"description"`
	PromoTitle       sql.NullString `db:"promo_title"`
	PromoDescription sql.NullString `db:"promo_description"`
	PromoKeywords    sql.NullString `db:"promo_keywords"`
	PromoRobots      sql.NullString `db:"promo_robots"`
}
