package model

import (
	"database/sql"
)

type Brand struct {
	ID               int64          `db:"id"`
	Code             string         `db:"code"`
	Name             string         `db:"name"`
	Header           sql.NullString `db:"header"`
	Description      sql.NullString `db:"description"`
	Country          sql.NullString `db:"country"`
	CountryID        sql.NullInt64  `db:"country_id"`
	PromoTitle       sql.NullString `db:"promo_title"`
	PromoDescription sql.NullString `db:"promo_description"`
	PromoKeywords    sql.NullString `db:"promo_keywords"`
	PromoRobots      sql.NullString `db:"promo_robots"`
}
