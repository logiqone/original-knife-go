package model

//go:generate reform

// StoreSlider represents a row in store_slider table.
//reform:store_slider
type StoreSlider struct {
	ID          int32   `reform:"id,pk"`
	Active      int8    `reform:"active"`
	Filename    string  `reform:"filename"`
	Title       string  `reform:"title"`
	Description string  `reform:"description"`
	Redirect    *string `reform:"redirect"`
}
