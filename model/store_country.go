package model

//go:generate reform

// StoreCountry represents a row in store_country table.
//reform:store_country
type StoreCountry struct {
	ID               int32   `reform:"id,pk"`
	CountryNameRu    string  `reform:"country_name_ru"`
	CountryNameEn    string  `reform:"country_name_en"`
	CountryCode      string  `reform:"country_code"`
	PromoTitle       *string `reform:"promo_title"`
	PromoDescription *string `reform:"promo_description"`
	PromoKeywords    *string `reform:"promo_keywords"`
	PromoRobots      *string `reform:"promo_robots"`
}
