package model

import "database/sql"

type ClassKnife struct {
	ID                  int32           `db:"id,pk"`
	ProductID           int32           `db:"product_id"`
	Color               sql.NullString  `db:"color"`
	BladeThickness      sql.NullFloat64 `db:"blade_thickness"`
	Grind               sql.NullString  `db:"grind"`
	HandleMaterial      sql.NullString  `db:"handle_material"`
	BladeMaterial       sql.NullString  `db:"blade_material"`
	SheathMaterial      sql.NullString  `db:"sheath_material"`
	LanyardHoleDiameter sql.NullFloat64 `db:"lanyard_hole_diameter"`
	BladeLength         sql.NullFloat64 `db:"blade_length"`
	BladeWidth          sql.NullFloat64 `db:"blade_width"`
	KnifeWidth          sql.NullFloat64 `db:"knife_width"`
	KnifeLength         sql.NullFloat64 `db:"knife_length"`
	HandleLength        sql.NullFloat64 `db:"handle_length"`
	BladeHardness       sql.NullInt64   `db:"blade_hardness"`
	TypeProcessingBlade sql.NullString  `db:"type_processing_blade"`
	BladeShape          sql.NullString  `db:"blade_shape"`
	ShapeSharpening     sql.NullString  `db:"shape_sharpening"`
	SharpeningAngle     sql.NullInt64   `db:"sharpening_angle"`
	SharpeningStone     sql.NullString  `db:"sharpening_stone"`
	SharpeningStages    sql.NullInt64   `db:"sharpening_stages"`
	LockType            sql.NullString  `db:"lock_type"`
	Clamp               bool            `db:"clamp"`
	Fuse                bool            `db:"fuse"`
	MechanismType       sql.NullString  `db:"mechanism_type"`
	NumberOfLayers      sql.NullInt64   `db:"number_of_layers"`
	Designer            sql.NullString  `db:"designer"`
}
