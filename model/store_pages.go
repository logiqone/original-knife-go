package model

//go:generate reform

// StorePages represents a row in store_pages table.
//reform:store_pages
type StorePages struct {
	ID              int32  `reform:"id,pk"`
	Title           string `reform:"title"`
	Desc            string `reform:"desc"`
	MetaTitle       string `reform:"meta_title"`
	MetaDescription string `reform:"meta_description"`
	MetaKeywords    string `reform:"meta_keywords"`
	MetaRobots      string `reform:"meta_robots"`
}
