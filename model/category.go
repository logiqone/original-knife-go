package model

import (
	"database/sql"
)

type Category struct {
	ID               int64          `db:"id"`
	Code             string         `db:"code"`
	ParentID         sql.NullInt64  `db:"parent_id"`
	Name             string         `db:"name"`
	Description      string         `db:"description"`
	PromoTitle       sql.NullString `db:"promo_title"`
	PromoDescription sql.NullString `db:"promo_description"`
	PromoKeywords    sql.NullString `db:"promo_keywords"`
	PromoRobots      sql.NullString `db:"promo_robots"`
	Sort             int64          `db:"sort"`
}
