package model

//go:generate reform

// StoreRelatedProduct represents a row in store_related_product table.
//reform:store_related_product
type StoreRelatedProduct struct {
	IDProduct        int32 `reform:"id_product"`
	IDProductRelated int32 `reform:"id_product_related"`
}
