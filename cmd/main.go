package main

import (
	"gitlab.com/logiqone/original-knife-go/workers/constructor"
	"gitlab.com/logiqone/original-knife-go/workers/dataprovider"
	"gitlab.com/logiqone/original-knife-go/workers/multiplexer"
	"gitlab.com/logiqone/original-knife-go/workers/server"
	"log"
)

func main() {
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)

	dp, err := dataprovider.NewDataProvider()
	if err != nil {
		log.Fatal(err)
	}

	c, err := constructor.NewConstructor(dp)
	if err != nil {
		log.Fatal(err)
	}

	m, err := multiplexer.NewMultiplexer(c)
	if err != nil {
		log.Fatal(err)
	}

	s := server.NewServer(m)

	log.Print("starting server...")
	if err := s.Start(); err != nil {
		log.Fatal(err)
	}
}
