$(document).ready(function() {
	$('ul#left_menu ul').each(function(i) {
  		if ($.cookie('submenuMark-' + i)) {
   			$(this).show().prev().removeClass('collapsed').addClass('expanded');
  		}
		else {
			$(this).hide().prev().removeClass('expanded').addClass('collapsed');
		}
        $(this).prev().prev().addClass('hasChildren').click(function() {
           var this_i = $('ul#left_menu ul').index($(this).next().next());
            if ($(this).next().next().css('display') == 'none') {
                $(this).parent('li').parent('ul').find('ul').each(function(j) {
                    if (j != this_i) {
                        $(this).slideUp(200, function () {
                            $(this).prev().removeClass('expanded').addClass('collapsed');
                            //cookieDel($('ul#left_menu ul').index($(this)));
                        });
                    }
                });

                $(this).next().next().slideDown(200, function () {
                    $(this).prev().removeClass('collapsed').addClass('expanded');
                    //cookieSet(this_i);
                });
            }
            else {
                $(this).next().next().slideUp(200, function () {
                    $(this).prev().removeClass('expanded').addClass('collapsed');
                    //cookieDel(this_i);
                    $(this).find('ul').each(function() {
                        $(this).hide().prev().removeClass('expanded').addClass('collapsed');
                    });
                });
            }
        });
	});





    $('ul#right_menu ul').each(function(i) {
        if ($.cookie('submenuMark-' + i)) {
            $(this).show().prev().removeClass('collapsed').addClass('expanded');
        }
        else {
            $(this).hide().prev().removeClass('expanded').addClass('collapsed');
        }

        $(this).prev().prev().addClass('hasChildren').click(function() {
            var this_i = $('ul#right_menu ul').index($(this).next().next());
            if ($(this).next().next().css('display') == 'none') {
                $(this).parent('li').parent('ul').find('ul').each(function(j) {
                    if (j != this_i) {
                        $(this).slideUp(200, function () {
                            $(this).prev().removeClass('expanded').addClass('collapsed');
                            //cookieDel($('ul#right_menu ul').index($(this)));
                        });
                    }
                });

                $(this).next().next().slideDown(200, function () {
                    $(this).prev().removeClass('collapsed').addClass('expanded');
                    //cookieSet(this_i);
                });
            }
            else {
                $(this).next().next().slideUp(200, function () {
                    $(this).prev().removeClass('expanded').addClass('collapsed');
                    //cookieDel(this_i);
                    $(this).find('ul').each(function() {
                        $(this).hide().prev().removeClass('expanded').addClass('collapsed');
                    });
                });
            }
            return false;
        });
    });







    var url = location.href;
    // Если строка содержит 'catalogue'
    if( url.indexOf('catalogue/') + 1) {
        var urlCutted = url.split('catalogue/')[1];
        var urlSplitted = urlCutted.split('/');
        // Если всего один параметр, то это основная категория
        if ( urlSplitted.length == 1) {
            // Раскровем основную категорию в меню
            //$(".categoryMenuLink.expanded").next().hide();
            $(".categoryMenuLink[data-code='"+urlSplitted[0]+"']").addClass("selected").next().show();
        }
        else if ( urlSplitted.length  == 2) {
            // Раскровем основную категорию в меню
            $(".categoryMenuLink[data-code='"+urlSplitted[0]+"']").addClass("selected").next().show();
            // Подсветим подкатегорию
            $(".subcategoryMenuLink[data-code='"+urlSplitted[1]+"']").addClass("selected");
        }
        else if ( urlSplitted.length  == 3) {
            // Раскровем основную категорию в меню
            $(".categoryMenuLink[data-code='"+urlSplitted[0]+"']").addClass("selected").next().show();
            // Подсветим бренд в правом меню
            $(".brandMenuLink[data-code='"+urlSplitted[2]+"']").addClass("selected").next().show();

            if ( urlCutted.indexOf('brand') == -1) {
                // Подсветим подкатегорию
                $(".subcategoryMenuLink[data-code='"+urlSplitted[1]+"']").addClass("selected");
            }
        }
    }

    // Если строка содержит 'brand'
    if( url.indexOf('brands/') + 1) {
        var urlCutted = url.split('brands/')[1];
        var urlSplitted = urlCutted.split('/');
        if ( urlSplitted.length == 1 && urlSplitted[0] != "") {
            $(".brandMenuLink[data-code='"+urlSplitted[0]+"']").addClass("selected").next().show();
        }
        else if ( urlSplitted.length == 2) {
            $(".brandMenuLink[data-code='"+urlSplitted[0]+"']").addClass("selected").next().show();
            $(".collectionMenuLink[data-code='"+urlSplitted[1]+"']").addClass("selected").next().show();
        }
    }

    // Если строка содержит 'product'
    if( url.indexOf('product/') + 1) {
        var urlCutted = url.split('product/')[1];
        var urlSplitted = urlCutted.split('/');

        $(".categoryMenuLink[data-code='"+$("#categoryCode").val()+"']").addClass("selected").next().show();
        $(".subcategoryMenuLink[data-code='"+$("#subcategoryCode").val()+"']").addClass("selected");

        $(".brandMenuLink[data-code='"+$("#brandCode").val()+"']").addClass("selected").next().show();
        if ( urlSplitted.length == 3 ) $(".subcategoryMenuLink[data-code='"+urlSplitted[1]+"']").addClass("selected").next().show();
    }
});

function cookieSet(index) {
	$.cookie('submenuMark-' + index, 'opened', {expires: null, path: '/'});
}

function cookieDel(index) {
	$.cookie('submenuMark-' + index, null, {expires: null, path: '/'});
}
