function cartOrangeButton(el, productId, price) {
    // Если нажата кнопка купить
    if( $(el).hasClass("deleteOrange") ){
        // delFromCart(productId, price);
        // $(el).html("Купить");
        window.location = $("#baseUrl").val() + "/cart";

        //$("a[data-id='ng_buy']").each(function(){
        //    $(this).removeAttr("class").html("Купить");
        //});
        //
        //$("#headerCartLink").fadeOut( 400 , function(){
        //    $("#headerCartLink").fadeIn( 400, function(){
        //        $("#headerCartLink").fadeOut( 400, function(){
        //            $("#headerCartLink").fadeIn( 400 );
        //        } );
        //    } );
        //});
    }
    else {
        ajaxAddToCart(productId, price);
        $(el).addClass("deleteOrange");
        $(el).html("В корзину");

        //$("a[data-id='ng_buy']").each(function(){
        //    $(this).addClass("deleteOrange").html("В корзину");
        //});

        // Эффект добавления в корзину
       // $(el).effect('transfer', { to: $('#search') }, 1000);

       // $(el).effect( "transfer", { to: $("#headerCartLink"), className: "ui-effects-transfer" }, 1000 );
        $(el).parent().find('.goods_img_place').effect( "transfer", { to: "#headerCartLink", className: "ui-effects-transfer" }, 1000 );

        $("#headerCartLink").fadeOut( 400 , function(){
            $("#headerCartLink").fadeIn( 400 );
        });
    }
}

$( document ).ready(function() {
    $('#a').click(function () {
        $(this).effect(
            'transfer',
            {to: $('#b')},
            1000
        )
    })
});



function ajaxAddToCart(productId, price) {
    $.ajax({
        type: "POST",
        data: "productId=" + productId + "&price=" + price + "&t=" + (new Date).getTime(),
        url: $("#baseUrl").val() + "/cart/ajaxAddToCart",
        success: function () {
            ajaxRenderHeaderCart();
        }
    });
}

function delFromCart(productId, price) {
    $.ajax({
        type: "POST",
        data: "productId=" + productId + "&price=" + price,
        url: $("#baseUrl").val() + "/cart/ajaxDelFromCart",
        success: function (html) {
            $("#fullcart").html(html);
            ajaxRenderHeaderCart();
        }
    });
}

function ajaxRenderHeaderCart() {
    $.ajax({
        data: "t=" + (new Date).getTime(),
        url: $("#baseUrl").val() + "/cart/ajaxRenderHeaderCart",
        success: function (html) {
            $("#header_cart").html(html);
        }
    });
}