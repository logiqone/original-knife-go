var timer = 0;

function catalogBarImageLink_mouseover(el)  {
    $(".previewImageLink").hide();
    var element = $(el);
    clearTimeout(timer);
    timer = setTimeout(function(){
        element.parent().next().fadeIn(200);
    },1000);
}

function catalogBarImageLink_mouseout()  {
    clearTimeout(timer);
    $(".previewImageLink").hide();
}

function previewImageLink_mouseout() {
    clearTimeout(timer);
    $(".previewImageLink").hide();
}
$(function() {
    $("#advancedSearch").click(function()
    {
        if( $("#advancedSearch").attr("data-show") == "0" )
        {
            $("#filter").slideDown();
            $("#advancedSearch").attr("data-show", "1" )
        }
        else
        {
            $("#filter").slideUp();
            $("#advancedSearch").attr("data-show", "0"  )
        }
    });


});

var top_show = 150; // В каком положении полосы прокрутки начинать показ кнопки "Наверх"
var delay = 500; // Задержка прокрутки
$(document).ready(function() {
    $(window).scroll(function () { // При прокрутке попадаем в эту функцию
        /* В зависимости от положения полосы прокрукти и значения top_show, скрываем или открываем кнопку "Наверх" */
        if ($(this).scrollTop() > top_show) $('div.toUp').fadeIn();
        else $('div.toUp').fadeOut();
    });
    $('div.toUp').click(function () { // При клике по кнопке "Наверх" попадаем в эту функцию
        /* Плавная прокрутка наверх */
        $('body, html').animate({
            scrollTop: 0
        }, delay);
    });

    $('select').on('change', function () {
        if( this.value !== "" ) {
            this.style.backgroundColor = "#FFAE00";
            this.style.border = "1px solid rgb(166, 166, 166)";
            this.style.borderRadius = "4px";
            this.style.color = "#fff";
            this.style.borderImage = "initial";
        }
        else {
            this.style.backgroundColor = null;
            this.style.color = null;
            this.style.borderWidth = "1px";
        }
    });
});




// Функция обратного звонка
function ajaxCallback() {
    $.ajax({
        type: "POST",
        data: "name=" + $("#cbname").val() + "&phone=" + $("#cbphone").val() + "&t=" + (new Date).getTime(),
        url: $("#baseUrl").val() + "/cart/ajaxCallback",
        success: function (html) {
            alert(html);
            $("#callback").fadeOut("slow");
        }
    });
}



