$(function() {
    $("div.nextImage").click( function(){
       $("#ng_big_desc_img a.fancy-inline").each(function(){
           if( !$(this).hasClass("invisible") )
           {
               $(this).addClass('invisible');
               var imageLenght = $( "#ng_big_desc_img a" ).size() - 1;
               var currentImage = $("#ng_big_desc_img a").index($(this));

               if ( currentImage ==  imageLenght) {
                   $("#ng_big_desc_img a").first().removeClass('invisible');
               }
               else {
                   $(this).next().removeClass('invisible');
               }
               return false
           }
       })
    });

    $("div.prevImage").click( function(){
        $("#ng_big_desc_img a").each(function(){
            if( !$(this).hasClass("invisible") )
            {
                $(this).hide();
                var currentImage = $("#ng_big_desc_img a").index($(this));

                if ( currentImage ==  0) {
                    $("#ng_big_desc_img a").last().fadeIn();
                }
                else {
                    $(this).prev().fadeIn();
                }
                return false
            }
        })
    });
});


$(document).ready(function(){
    setInterval(ajaxRefreshSimilar, 10000);
    setInterval(ajaxRefreshRelated, 13000);
    setInterval(ajaxRefreshRecentlyViewed , 16000);

    //$('.fancy-inline').fancybox({
    //    beforeShow : function(){
    //        alert($(this.element).attr('title'));
    //        this.title =  $(this.element).title
    //    }
    //});
});


function ajaxRefreshSimilar() {
    var data = "currentPage=" + $("#similarListView .info.currentPage").html() + "&lastPage=" + $("#similarListView .info.lastPage").html() + "&productId=" + $("#productId").val();
    //alert(data);
    $.ajax({
        type: "POST",
        data: data,
        url: $("#baseUrl").val() + "/product/AjaxRefreshSimilar",
        success: function (html) {

            $("#similarListView").hide();
            $("#similarListView").html(html);
            $("#similarListView").fadeIn(1000);

            var timer = 0;
            $("#similarListView .catalogImageLink").mouseover( function() {
                $("#similarListView .previewImageLink").hide();
                var element = $(this);
                clearTimeout(timer);
                timer = setTimeout(function(){
                    element.parent().next().fadeIn(200);
                },1000);
            }).mouseout( function() {
                clearTimeout(timer);
                $("#similarListView .previewImageLink").hide();
            });
            $("#similarListView .previewImageLink").mouseout( function() {
                clearTimeout(timer);
                $("#similarListView .previewImageLink").hide();
            });

        }
    });
}

function ajaxRefreshRelated() {
    var data = "currentPage=" + $("#relatedListView .info.currentPage").html() + "&lastPage=" + $("#relatedListView .info.lastPage").html() + "&productId=" + $("#productId").val();
    // alert(data);
    $.ajax({
        type: "POST",
        data: data,
        url: $("#baseUrl").val() + "/product/AjaxRefreshRelated",
        success: function (html) {

            $("#relatedListView").hide();
            $("#relatedListView").html(html);
            $("#relatedListView").fadeIn(1000);

            var timer = 0;
            $("#relatedListView .catalogImageLink").mouseover( function() {
                $("#relatedListView .previewImageLink").hide();
                var element = $(this);
                clearTimeout(timer);
                timer = setTimeout(function(){
                    element.parent().next().fadeIn(200);
                },1000);
            }).mouseout( function() {
                clearTimeout(timer);
                $("#relatedListView .previewImageLink").hide();
            });
            $("#relatedListView .previewImageLink").mouseout( function() {
                clearTimeout(timer);
                $("#relatedListView .previewImageLink").hide();
            });

            // discountsListView
            //setTimeout(ajaxRefreshDiscounts, 10000);
        }
    });
}

function ajaxRefreshRecentlyViewed() {
    $.ajax({
        type: "POST",
        data: "currentPage=" + $("#recentlyViewedListView .info.currentPage").html() + "&lastPage=" + $("#recentlyViewedListView .info.lastPage").html(),
        url: $("#baseUrl").val() + "/product/AjaxRefreshRecently",
        success: function (html) {

            $("#recentlyViewedListView").hide();
            $("#recentlyViewedListView").html(html);
            $("#recentlyViewedListView").fadeIn(1000);

            var timer = 0;
            $("#recentlyViewedListView .catalogImageLink").mouseover(function () {
                $("#recentlyViewedListView .previewImageLink").hide();
                var element = $(this);
                clearTimeout(timer);
                timer = setTimeout(function () {
                    element.parent().next().fadeIn(200);
                }, 1000);
            }).mouseout(function () {
                clearTimeout(timer);
                $("#recentlyViewedListView .previewImageLink").hide();
            });
            $("#recentlyViewedListView .previewImageLink").mouseout(function () {
                clearTimeout(timer);
                $("#recentlyViewedListView .previewImageLink").hide();
            });

            //setTimeout(ajaxRefreshBestsellers, 10000);
        }
    });
}

function oneClickIncreaseQuantity(price) {
    var currentQuantity = parseInt( $("#oneClickQuantity").html() );
    var currentTotalPrice = parseInt($("#oneClickTotalPrice").html());
    $("#oneClickQuantity").html(currentQuantity + 1);
    $("#oneClickTotalPrice").html(currentTotalPrice + parseInt(price));
}

function oneClickDecreaseQuantity(price) {
    var currentQuantity = parseInt( $("#oneClickQuantity").html() );
    if (currentQuantity > 1) {
        var currentTotalPrice = parseInt($("#oneClickTotalPrice").html());
        $("#oneClickQuantity").html(currentQuantity - 1);
        $("#oneClickTotalPrice").html(currentTotalPrice - parseInt(price));
    }
}

function oneClickOrder() {
    var $form = $('#order-one-click-form'), settings = $form.data('settings');
    settings.submitting = true;
    $.fn.yiiactiveform.validate($form, function(mes) {
        if($.isEmptyObject(mes)) { // valid form
            var data = $('#order-one-click-form').serialize() + "&productId=" + $("#productId").val() + "&productQuantity=" + $("#oneClickQuantity").html() + "&total=" + $("#oneClickTotalPrice").html();
            $.ajax({
                type: "POST",
                data: data,
                url: $("#baseUrl").val() + "/product/AjaxOneClickOrder",
                success: function (html) {
                    alert(html);
                    $('#oneClickOrder').fadeOut('slow');
                }
            });

        } else {  // invalid
            alert(mes[Object.keys(mes)[0]]);
        }
    });
}