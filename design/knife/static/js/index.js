Vue.use(VueAwesomeSwiper)
new Vue({
    el: '#vueapp',
    components: {
        LocalSwiper: VueAwesomeSwiper.swiper,
        LocalSlide: VueAwesomeSwiper.swiperSlide,
    },
    data: {
        message: 'Hi from Vue',
        swiperOption: {
            pagination: {
                el: '.swiper-pagination'
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            },
        },
    },
    computed: {
        swiper() {
            return this.$refs.awesomeSwiperA.swiper
        },
    },
    methods: {
        onSetTranslate() {
            console.log('onSetTranslate')
        }
    },
    mounted() {
        console.log('this is swiper A instance object', this.swiperA, 'B instance', this.swiperB)
    }
})