$(document).ready(function(){
    //alert('Страница загружена!');
});

$(function() {
    //$(".flash-success").animate({opacity: 1.0}, 5000).fadeOut("slow");
});

function ajaxRenderHeaderCart() {
    $.ajax({
        url: $("#baseUrl").val() + "/cart/ajaxRenderHeaderCart",
        success: function (html) {
            $("#header_cart").html(html);
        }
    });
} 

function delFromCart(productId, price) {
    $.ajax({
        type: "POST",
        data: "productId=" + productId + "&price=" + price,
        url: $("#baseUrl").val() + "/cart/ajaxDelFromCart",
        success: function (html) {
            $("#fullcart").html(html);
            ajaxRenderHeaderCart();

            $("#headerCartLink").fadeOut( 400 , function(){
                $("#headerCartLink").fadeIn( 400 );
            });

            // Убрать форму заказа, если корзина пустая
            if (!getCookie("storeCubeProducts")) $("#userform").hide();
        }
    });
}

function increaseQuantity(productId, price) {
    $.ajax({
        type: "POST",
        data: "productId=" + productId + "&price=" + price,
        url: $("#baseUrl").val() + "/cart/ajaxIncreaseQuantity",
        success: function (html) {
            $("#fullcart").html(html);

            $("#headerCartLink").fadeOut( 400 , function(){
                $("#headerCartLink").fadeIn( 400 );
            });

            ajaxRenderHeaderCart();
        }
    });
}

function decreaseQuantity(productId, price) {
    $.ajax({
        type: "POST",
        data: "productId=" + productId + "&price=" + price,
        url: $("#baseUrl").val() + "/cart/ajaxDecreaseQuantity",
        success: function (html) {
            $("#fullcart").html(html);

            $("#headerCartLink").fadeOut( 400 , function(){
                $("#headerCartLink").fadeIn( 400 );
            });

            ajaxRenderHeaderCart();
        }
    });
}


function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}
