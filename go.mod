module gitlab.com/logiqone/original-knife-go

go 1.12

require (
	github.com/AlekSi/pointer v1.0.0 // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20190707035753-2be1aa521ff4 // indirect
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/mux v1.6.2
	github.com/jmoiron/sqlx v1.2.0
	github.com/julienschmidt/httprouter v1.2.0
	github.com/kisielk/godepgraph v0.0.0-20190626013829-57a7e4a651a9 // indirect
	github.com/lib/pq v1.1.1 // indirect
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	gopkg.in/reform.v1 v1.3.3
)
