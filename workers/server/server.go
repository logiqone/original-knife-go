package server

import (
	"gitlab.com/logiqone/original-knife-go/workers/multiplexer"
	"net/http"
	"time"
)

type Serve interface {
	Start() error
	Stop() error
}

type Server struct {
	instance *http.Server
}

// /Users/logiq/go/src/gitlab.com/logiqone/original-knife-go
func NewServer(multiplexer *multiplexer.Multiplexer) *Server {
	// TODO CREATE CONFIG !!
	s := &Server{}
	s.instance = &http.Server{
		Addr:           ":3000",
		Handler:        multiplexer.Router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	return s
}

func (s *Server) Start() error {
	return s.instance.ListenAndServe()
}

func (s *Server) Stop() error {
	return s.instance.Close()
}
