package multiplexer

import (
	"github.com/gorilla/mux"
	"gitlab.com/logiqone/original-knife-go/model"
	"gitlab.com/logiqone/original-knife-go/workers/constructor"
	"html/template"
	"log"
	"net/http"
)

type PageSubcategoryContext struct {
	PromoTitle             string
	PromoDescription       string
	PromoKeywords          string
	Category               *model.Category
	Subcategory            *model.Category
	SubcategoryDescription template.HTML
	Brands                 *[]model.Brand
	Products               *[]model.Product
	MenuLeft               []*constructor.MenuElement
	MenuRight              []*constructor.MenuElement
}

func (m *Multiplexer) subcategory(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	subcategory, err := m.Constructor.DataProvider.GetCategoryByCode(vars["subcategory"])
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("не удалось получить информацию о сабкатегории, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	category, err := m.Constructor.DataProvider.GetCategoryById(subcategory.ParentID.Int64)
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("не удалось получить информацию о сабкатегории, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	brands, err := m.Constructor.DataProvider.GetBrandsByCategoryId(subcategory.ID)
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("не удалось получить информацию о брендах, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	// todo getproducts by subcategory id
	products, err := m.Constructor.DataProvider.GetProductsByCategoryId(subcategory.ID)
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("не удалось получить информацию о брендах, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	// todo forming title
	context := PageSubcategoryContext{
		PromoTitle:             subcategory.PromoTitle.String,
		Brands:                 brands,
		Category:               category,
		Subcategory:            subcategory,
		SubcategoryDescription: template.HTML(subcategory.Description),
		Products:               products,
		MenuLeft:               m.Constructor.MenuLeft,
		MenuRight:              m.Constructor.MenuRight,
	}

	templates, err := template.ParseFiles(
		"design/knife/templates/layouts/main.html",
		"design/knife/templates/component/head.html",
		"design/knife/templates/component/statistic.html",
		"design/knife/templates/component/menu_left.html",
		"design/knife/templates/component/menu_right.html",
		"design/knife/templates/component/products_view_bar.html",
		"design/knife/templates/pages/subcategory.html",
	)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := templates.Execute(w, context); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
