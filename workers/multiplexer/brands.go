package multiplexer

import (
	"gitlab.com/logiqone/original-knife-go/model"
	"gitlab.com/logiqone/original-knife-go/workers/constructor"
	"net/http"
)

type PageBrandsContext struct {
	PromoTitle     string
	MenuLeft       []*constructor.MenuElement
	MenuRight      []*constructor.MenuElement
	Novelties      *[]model.Product
	RootCategories *[]model.Category
	Brands         *[]model.Brand
}

func (m *Multiplexer) brands(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("brands"))
	return
}
