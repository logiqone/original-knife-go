package multiplexer

import (
	"gitlab.com/logiqone/original-knife-go/model"
	"gitlab.com/logiqone/original-knife-go/workers/constructor"
	"html/template"
	"log"
	"net/http"
)

type PageIndexContext struct {
	PromoTitle     string
	MenuLeft       []*constructor.MenuElement
	MenuRight      []*constructor.MenuElement
	Novelties      *[]model.Product
	RootCategories *[]model.Category
	Brands         *[]model.Brand
}

func (m *Multiplexer) index(w http.ResponseWriter, r *http.Request) {
	if r.URL.String() != "/" {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	novelties, err := m.Constructor.DataProvider.GetNovelties()
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("не удалось получить новинки из базы, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	rootCategories, err := m.Constructor.DataProvider.GetRootCategories()
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("не удалось получить корневые категории из базы, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	brands, err := m.Constructor.DataProvider.GetAllBrands()
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("не удалось получить корневые категории из базы, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	context := PageIndexContext{
		PromoTitle:     "Купить нож в Москве. Гипермаркет ножей в Москве",
		MenuLeft:       m.Constructor.MenuLeft,
		MenuRight:      m.Constructor.MenuRight,
		Novelties:      novelties,
		RootCategories: rootCategories,
		Brands:         brands,
	}

	templates, err := template.ParseFiles(
		"design/knife/templates/layouts/main.html",
		"design/knife/templates/component/head.html",
		"design/knife/templates/component/statistic.html",
		"design/knife/templates/component/menu_left.html",
		"design/knife/templates/component/menu_right.html",
		"design/knife/templates/pages/index.html",
	)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := templates.Execute(w, context); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
