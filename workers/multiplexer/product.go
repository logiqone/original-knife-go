package multiplexer

import (
	"github.com/gorilla/mux"
	"gitlab.com/logiqone/original-knife-go/model"
	"gitlab.com/logiqone/original-knife-go/workers/constructor"
	"html/template"
	"log"
	"net/http"
)

type PageProductContext struct {
	PromoTitle       string
	PromoDescription string
	PromoKeywords    string
	MenuLeft         []*constructor.MenuElement
	MenuRight        []*constructor.MenuElement
	Product          *model.Product
	Brand            *model.Brand
	ClassKnife       *model.ClassKnife
	ClassType        *model.ClassType
	Categories       *[]model.Category
	Collection       *model.Collection
	Similar          *[]model.Product
	Related          *[]model.Product
	Recently         *[]model.Product
	Discount         *model.Discount
	Url              string
}

func (m *Multiplexer) product(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	product, err := m.Constructor.DataProvider.GetProductByCode(vars["product"])
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("Разработчику уже автоматически отправлено сообщение об ошибке!"))
		return
	}

	classKnife, _ := m.Constructor.DataProvider.GetClassKnifeByProductId(product.ID)

	classType, _ := m.Constructor.DataProvider.GetClassTypeByProductId(product.ID)

	collection, _ := m.Constructor.DataProvider.GetCollectionById(product.CollectionID.Int64)

	brand, err := m.Constructor.DataProvider.GetBrandByCode(vars["brand"])
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("Разработчику уже автоматически отправлено сообщение об ошибке!"))
		return
	}

	context := PageProductContext{
		PromoTitle: product.PromoTitle.String,
		Product:    product,
		Brand:      brand,
		Collection: collection,
		ClassKnife: classKnife,
		ClassType:  classType,
		MenuLeft:   m.Constructor.MenuLeft,
		MenuRight:  m.Constructor.MenuRight,
		Url:        "http://originl-knife.ru/",
	}

	templates, err := template.ParseFiles(
		"design/knife/templates/layouts/main.html",
		"design/knife/templates/component/head.html",
		"design/knife/templates/component/statistic.html",
		"design/knife/templates/component/menu_left.html",
		"design/knife/templates/component/menu_right.html",
		"design/knife/templates/component/products_view_bar.html",
		"design/knife/templates/pages/product.html",
	)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := templates.Execute(w, context); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
