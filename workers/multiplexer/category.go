package multiplexer

import (
	"github.com/gorilla/mux"
	"gitlab.com/logiqone/original-knife-go/model"
	"gitlab.com/logiqone/original-knife-go/workers/constructor"
	"html/template"
	"log"
	"net/http"
)

type PageCategoryContext struct {
	PromoTitle          string
	PromoDescription    string
	PromoKeywords       string
	Category            *model.Category
	CategoryDescription template.HTML
	Subcategories       *[]model.Category
	Brands              *[]model.Brand
	Products            *[]model.Product
	MenuLeft            []*constructor.MenuElement
	MenuRight           []*constructor.MenuElement
}

// Category page
func (m *Multiplexer) category(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	category, err := m.Constructor.DataProvider.GetCategoryByCode(vars["category"])
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("не удалось получить информацию о категории, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	subcategories, err := m.Constructor.DataProvider.GetSubcategoriesByParentId(category.ID)
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("не удалось получить информацию о подкатегории, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	brands, err := m.Constructor.DataProvider.GetBrandsByCategoryId(category.ID)
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("не удалось получить информацию о брендах, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	products, err := m.Constructor.DataProvider.GetProductsByCategoryId(category.ID)
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("не удалось получить информацию о брендах, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	// todo forming title
	context := PageCategoryContext{
		PromoTitle:          category.PromoTitle.String,
		Category:            category,
		Subcategories:       subcategories,
		Brands:              brands,
		Products:            products,
		CategoryDescription: template.HTML(category.Description),
		MenuLeft:            m.Constructor.MenuLeft,
		MenuRight:           m.Constructor.MenuRight,
	}

	templates, err := template.ParseFiles(
		"design/knife/templates/layouts/main.html",
		"design/knife/templates/component/head.html",
		"design/knife/templates/component/statistic.html",
		"design/knife/templates/component/menu_left.html",
		"design/knife/templates/component/menu_right.html",
		"design/knife/templates/component/products_view_bar.html",
		"design/knife/templates/pages/category.html",
	)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := templates.Execute(w, context); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
