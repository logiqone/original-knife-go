package multiplexer

import (
	"github.com/gorilla/mux"
	"gitlab.com/logiqone/original-knife-go/workers/constructor"
	"net/http"
)

type Multiplexer struct {
	Router      *mux.Router
	Constructor *constructor.Constructor
}

func NewMultiplexer(constructor *constructor.Constructor) (m *Multiplexer, err error) {
	m = &Multiplexer{}

	m.Router = mux.NewRouter()

	m.Router.PathPrefix("/photo/").Handler(http.StripPrefix("/photo/", http.FileServer(http.Dir("./photo/"))))
	m.Router.PathPrefix("/public/").Handler(http.StripPrefix("/public/", http.FileServer(http.Dir("./design/knife/static/"))))

	m.Router.HandleFunc("/", m.index)                                         // 50%
	m.Router.HandleFunc("/catalogue", m.catalogue)                            // 50%
	m.Router.HandleFunc("/catalogue/{category}", m.category)                  // 50%
	m.Router.HandleFunc("/catalogue/{category}/{subcategory}", m.subcategory) // 50%

	// 'catalogue/<category_code>/brand/<brand_code>/' => 'catalog/categorybrand'
	// 'catalogue/<category_code>/<subcategory_code>/<brand_code>/' => 'catalog/subcategorybrand'
	m.Router.HandleFunc("/catalogue/{category}/brand/{brand}", m.categoryBrand)
	m.Router.HandleFunc("/catalogue/{category}/{subcategory}/{brand}", m.subcategoryBrand)

	// 'categories' => 'catalog/categories',
	// 'brands' => 'catalog/brands',
	m.Router.HandleFunc("/categories", m.categories)
	m.Router.HandleFunc("/brands", m.brands)

	//'brands/<brand_code>' => 'catalog/brand',
	//'brands/<brand_code>/<collection_code>' => 'catalog/collection',
	m.Router.HandleFunc("/brands/{brand}", m.brand)
	m.Router.HandleFunc("/brands/{brand}/{collection}", m.collection)

	// 'product/<brand_code>/<product_code>.html' => 'product/view',
	// 'country/<country_name_en>' => 'catalog/country',
	m.Router.HandleFunc("/product/{brand}/{product}.html", m.product)
	m.Router.HandleFunc("/country/{country}", m.country)

	m.Constructor = constructor
	return m, nil
}
