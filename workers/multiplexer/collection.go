package multiplexer

import (
	"github.com/gorilla/mux"
	"gitlab.com/logiqone/original-knife-go/model"
	"gitlab.com/logiqone/original-knife-go/workers/constructor"
	"html/template"
	"log"
	"net/http"
)

type PageCollectionContext struct {
	PromoTitle            string
	MenuLeft              []*constructor.MenuElement
	MenuRight             []*constructor.MenuElement
	Brand                 *model.Brand
	Collection            *model.Collection
	CollectionDescription template.HTML
	Products              *[]model.Product
	RootCategories        *[]model.Category
	Brands                *[]model.Brand
}

func (m *Multiplexer) collection(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	brand, err := m.Constructor.DataProvider.GetBrandByCode(vars["brand"])
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("не удалось получить информацию о брендах, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	collection, err := m.Constructor.DataProvider.GetCollectionById(brand.ID)
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("не удалось получить информацию о брендах, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	products, err := m.Constructor.DataProvider.GetProductsByCollectionId(collection.ID)
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("не удалось получить информацию о брендах, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	// todo forming title
	context := PageCollectionContext{
		PromoTitle:            collection.PromoTitle.String,
		Brand:                 brand,
		Collection:            collection,
		CollectionDescription: template.HTML(collection.Description),
		Products:              products,
		MenuLeft:              m.Constructor.MenuLeft,
		MenuRight:             m.Constructor.MenuRight,
	}

	templates, err := template.ParseFiles(
		"design/knife/templates/layouts/main.html",
		"design/knife/templates/component/head.html",
		"design/knife/templates/component/statistic.html",
		"design/knife/templates/component/menu_left.html",
		"design/knife/templates/component/menu_right.html",
		"design/knife/templates/component/products_view_bar.html",
		"design/knife/templates/pages/collection.html",
	)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := templates.Execute(w, context); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
