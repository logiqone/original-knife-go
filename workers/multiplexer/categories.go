package multiplexer

import (
	"gitlab.com/logiqone/original-knife-go/model"
	"gitlab.com/logiqone/original-knife-go/workers/constructor"
	"net/http"
)

type PageCategoriesContext struct {
	PromoTitle     string
	MenuLeft       []*constructor.MenuElement
	MenuRight      []*constructor.MenuElement
	Novelties      *[]model.Product
	RootCategories *[]model.Category
	Brands         *[]model.Brand
}

func (m *Multiplexer) categories(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("categories"))
	return
}
