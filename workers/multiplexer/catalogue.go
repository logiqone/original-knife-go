package multiplexer

import (
	"gitlab.com/logiqone/original-knife-go/model"
	"gitlab.com/logiqone/original-knife-go/workers/constructor"
	"html/template"
	"log"
	"net/http"
)

type PageCatalogContext struct {
	Title      string
	Categories *[]model.Category
	Brands     *[]model.Brand
	MenuLeft   []*constructor.MenuElement
	MenuRight  []*constructor.MenuElement
}

func (m *Multiplexer) catalogue(w http.ResponseWriter, r *http.Request) {
	categories, err := m.Constructor.DataProvider.GetRootCategories()
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("не удалось получить категории из базы, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	brands, err := m.Constructor.DataProvider.GetAllBrands()
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("не удалось получить категории из базы, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	context := PageCatalogContext{
		Title:      "Каталог",
		Categories: categories,
		Brands:     brands,
		MenuLeft:   m.Constructor.MenuLeft,
		MenuRight:  m.Constructor.MenuRight,
	}

	templates, err := template.ParseFiles(
		"design/knife/templates/layouts/main.html",
		"design/knife/templates/component/head.html",
		"design/knife/templates/component/statistic.html",
		"design/knife/templates/component/menu_left.html",
		"design/knife/templates/component/menu_right.html",
		"design/knife/templates/pages/catalogue.html",
	)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := templates.Execute(w, context); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
