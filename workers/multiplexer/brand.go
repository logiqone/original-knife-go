package multiplexer

import (
	"github.com/gorilla/mux"
	"gitlab.com/logiqone/original-knife-go/model"
	"gitlab.com/logiqone/original-knife-go/workers/constructor"
	"html/template"
	"log"
	"net/http"
)

type PageBrandContext struct {
	PromoTitle       string
	PromoDescription string
	PromoKeywords    string
	Brand            *model.Brand
	Collections      *[]model.Collection
	Products         *[]model.Product
	MenuLeft         []*constructor.MenuElement
	MenuRight        []*constructor.MenuElement
}

// Brand page
func (m *Multiplexer) brand(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	// GetBrandByCode
	brand, err := m.Constructor.DataProvider.GetBrandByCode(vars["brand"])
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("не удалось получить информацию о бренде, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	products, err := m.Constructor.DataProvider.GetProductsByBrandCode(vars["brand"])
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("не удалось получить информацию о бренде, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	collections, err := m.Constructor.DataProvider.GetCollectionsByBrandId(brand.ID)
	if err != nil {
		log.Printf("ERR: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("не удалось получить информацию о бренде, пожалуйста, " +
			"сообщите об этом разработчику: romanov.aero@gmail.com"))
		return
	}

	// todo forming title
	context := PageBrandContext{
		PromoTitle:       brand.PromoTitle.String,
		PromoDescription: brand.PromoDescription.String,
		PromoKeywords:    brand.PromoKeywords.String,
		Brand:            brand,
		Products:         products,
		Collections:      collections,
		MenuLeft:         m.Constructor.MenuLeft,
		MenuRight:        m.Constructor.MenuRight,
	}

	templates, err := template.ParseFiles(
		"design/knife/templates/layouts/main.html",
		"design/knife/templates/component/head.html",
		"design/knife/templates/component/statistic.html",
		"design/knife/templates/component/menu_left.html",
		"design/knife/templates/component/menu_right.html",
		"design/knife/templates/component/products_view_bar.html",
		"design/knife/templates/pages/brand.html",
	)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := templates.Execute(w, context); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
