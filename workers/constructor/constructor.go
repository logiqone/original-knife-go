package constructor

import (
	"gitlab.com/logiqone/original-knife-go/workers/dataprovider"
)

type Constructor struct {
	DataProvider dataprovider.DataProvide
	MenuLeft     []*MenuElement
	MenuRight    []*MenuElement
}

type MenuElement struct {
	Code       string
	ParentCode string
	Name       string
	IsRoot     bool
	HasChild   bool
	IsFirst    bool
	IsLast     bool
}

func NewConstructor(dataProvider dataprovider.DataProvide) (c *Constructor, err error) {
	c = &Constructor{
		DataProvider: dataProvider,
	}

	err = c.MakeLeftMenu()
	if err != nil {
		return nil, err
	}

	err = c.MakeRightMenu()
	if err != nil {
		return nil, err
	}

	return c, nil
}

func (c *Constructor) MakeLeftMenu() error {
	categories, err := c.DataProvider.GetAllCategories()
	if err != nil {
		return err
	}

	c.MenuLeft = make([]*MenuElement, len(*categories))
	var i = 0
	for _, root := range *categories {
		if !root.ParentID.Valid {
			c.MenuLeft[i] = &MenuElement{
				Code:   root.Code,
				Name:   root.Name,
				IsRoot: true,
			}

			var j = 1
			for _, child := range *categories {
				if child.ParentID.Valid && child.ParentID.Int64 == root.ID {
					c.MenuLeft[i+j] = &MenuElement{
						Code:       child.Code,
						ParentCode: root.Code,
						Name:       child.Name,
						IsRoot:     false,
					}
					if j == 1 {
						c.MenuLeft[i+j].IsFirst = true
					}
					j++
					c.MenuLeft[i].HasChild = true
				}
			}

			if c.MenuLeft[i].HasChild == true {
				i += j
				//log.Printf("i: %d ", i)
				c.MenuLeft[i-1].IsLast = true
			} else {
				i++
			}
		}
	}

	return nil
}

func (c *Constructor) MakeRightMenu() error {
	brands, err := c.DataProvider.GetAllBrands()
	if err != nil {
		return err
	}

	collections, err := c.DataProvider.GetAllCollections()
	if err != nil {
		return err
	}

	c.MenuRight = make([]*MenuElement, len(*collections)+len(*brands))

	var i = 0
	for _, root := range *brands {
		c.MenuRight[i] = &MenuElement{
			Code:   root.Code,
			Name:   root.Name,
			IsRoot: true,
		}

		var j = 1
		for _, child := range *collections {
			if child.BrandID == root.ID {
				c.MenuRight[i+j] = &MenuElement{
					Code:       child.Code,
					ParentCode: root.Code,
					Name:       child.Name,
					IsRoot:     false,
				}
				if j == 1 {
					c.MenuRight[i+j].IsFirst = true
				}
				j++
				c.MenuRight[i].HasChild = true
			}
		}

		if c.MenuRight[i].HasChild == true {
			i += j
			//log.Printf("i: %d ", i)
			c.MenuRight[i-1].IsLast = true
		} else {
			i++
		}
	}

	//for i := range c.MenuRight {
	//	log.Printf("i: %d, %v\n", i, c.MenuRight[i])
	//}

	return nil
}
