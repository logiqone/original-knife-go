package dataprovider

import "gitlab.com/logiqone/original-knife-go/model"

func (d *DataProvider) GetAllCategories() (*[]model.Category, error) {
	var categories []model.Category
	err := d.Database.Select(&categories, "SELECT * FROM original.store_category ORDER BY sort")
	if err != nil {
		return nil, err
	}
	return &categories, nil
}

func (d *DataProvider) GetRootCategories() (*[]model.Category, error) {
	var categories []model.Category
	err := d.Database.Select(&categories, "SELECT * FROM original.store_category WHERE parent_id is NULL ORDER BY sort")
	if err != nil {
		return nil, err
	}
	return &categories, nil
}

func (d *DataProvider) GetCategoryById(id int64) (*model.Category, error) {
	var subcategory model.Category
	err := d.Database.Get(&subcategory, `
			SELECT * FROM original.store_category WHERE id = ?
		`, id)
	if err != nil {
		return nil, err
	}
	return &subcategory, nil
}

// todo fix array
func (d *DataProvider) GetCategoryByCode(code string) (*model.Category, error) {
	var category model.Category
	err := d.Database.Get(&category, "SELECT * FROM original.store_category WHERE code=?", code)
	if err != nil {
		return nil, err
	}
	return &category, nil
}

func (d *DataProvider) GetSubcategoriesByParentId(id int64) (*[]model.Category, error) {
	var subcategories []model.Category
	err := d.Database.Select(&subcategories, "SELECT * FROM original.store_category WHERE parent_id=?", id)
	if err != nil {
		return nil, err
	}
	return &subcategories, nil
}
