package dataprovider

import "gitlab.com/logiqone/original-knife-go/model"

func (d *DataProvider) GetAllCollections() (*[]model.Collection, error) {
	var collections []model.Collection
	err := d.Database.Select(&collections, "SELECT * FROM original.store_group_collection")
	if err != nil {
		return nil, err
	}
	return &collections, nil
}

func (d *DataProvider) GetCollectionsByBrandId(id int64) (*[]model.Collection, error) {
	var collections []model.Collection
	err := d.Database.Select(&collections, `
		SELECT * 
		FROM original.store_group_collection
		WHERE original.store_group_collection.brand_id = ?
		`, id)
	if err != nil {
		return nil, err
	}
	return &collections, nil
}

func (d *DataProvider) GetCollectionById(id int64) (*model.Collection, error) {
	var collection model.Collection
	err := d.Database.Get(&collection, `
		SELECT * 
		FROM original.store_group_collection
		WHERE original.store_group_collection.brand_id = ?
		`, id)
	if err != nil {
		return nil, err
	}
	return &collection, nil
}
