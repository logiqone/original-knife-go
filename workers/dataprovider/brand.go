package dataprovider

import "gitlab.com/logiqone/original-knife-go/model"

func (d *DataProvider) GetAllBrands() (*[]model.Brand, error) {
	var brands []model.Brand
	err := d.Database.Select(&brands, "SELECT * FROM original.store_brand ORDER BY name")
	if err != nil {
		return nil, err
	}
	return &brands, nil
}

func (d *DataProvider) GetBrandByCode(code string) (*model.Brand, error) {
	var b model.Brand
	err := d.Database.Get(&b, `
			SELECT * 
			FROM original.store_brand 
			WHERE original.store_brand.code = ?
		`, code)
	if err != nil {
		return nil, err
	}
	return &b, nil
}

func (d *DataProvider) GetBrandsByCategoryId(id int64) (*[]model.Brand, error) {
	var brands []model.Brand
	err := d.Database.Select(&brands, `
		SELECT distinct original.store_brand.*
		FROM
			original.store_brand,
			original.store_product,
			original.store_product_to_category,
			original.store_category
		WHERE original.store_category.id = original.store_product_to_category.category_id
		  AND original.store_product.id = original.store_product_to_category.product_id
		  AND original.store_brand.id = original.store_product.brand_id
		  AND ( original.store_category.id = ? OR
				original.store_category.id IN (
					SELECT original.store_category.id
					FROM original.store_category
					WHERE original.store_category.parent_id = ?
				)
			)
`, id, id)
	if err != nil {
		return nil, err
	}
	return &brands, nil
}

// Harder than GetBrandsByCategoryId
func (d *DataProvider) GetBrandsByCategoryCode(code string) (*[]model.Brand, error) {
	var brands []model.Brand
	err := d.Database.Select(&brands, `
		SELECT distinct original.store_brand.*
		FROM
			original.store_brand,
			original.store_product,
			original.store_product_to_category,
			original.store_category
		WHERE original.store_category.id = original.store_product_to_category.category_id
		  AND original.store_product.id = original.store_product_to_category.product_id
		  AND original.store_brand.id = original.store_product.brand_id
		  AND ( original.store_category.code = ? OR
				original.store_category.id IN (
					SELECT original.store_category.id
					FROM original.store_category
					WHERE original.store_category.parent_id = (
							SELECT original.store_category.id FROM original.store_category WHERE original.store_category.code = ?
						)
				)
			)
`, code, code)
	if err != nil {
		return nil, err
	}
	return &brands, nil
}
