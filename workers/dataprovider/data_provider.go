package dataprovider

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"gitlab.com/logiqone/original-knife-go/model"
)

type DataProvide interface {
	GetAllCategories() (*[]model.Category, error)

	GetAllBrands() (*[]model.Brand, error)
	GetBrandByCode(string) (*model.Brand, error)
	GetBrandsByCategoryId(int64) (*[]model.Brand, error)
	GetBrandsByCategoryCode(string) (*[]model.Brand, error)

	GetAllCollections() (*[]model.Collection, error)
	GetCollectionsByBrandId(int64) (*[]model.Collection, error)
	GetCollectionById(int64) (*model.Collection, error)

	GetProductsByCategoryId(int64) (*[]model.Product, error)
	GetProductsByCategoryIdAndBrandId(int64, int64) (*[]model.Product, error)
	GetProductsByBrandCode(string) (*[]model.Product, error)
	GetProductsByCollectionId(int64) (*[]model.Product, error)
	GetNovelties() (*[]model.Product, error)
	GetProductByCode(string) (*model.Product, error)
	GetClassKnifeByProductId(int64) (*model.ClassKnife, error)
	GetClassTypeByProductId(int64) (*model.ClassType, error)

	GetRootCategories() (*[]model.Category, error)
	GetCategoryByCode(string) (*model.Category, error)

	GetCategoryById(int64) (*model.Category, error)
	GetSubcategoriesByParentId(int64) (*[]model.Category, error)
}

type DataProvider struct {
	Database *sqlx.DB
}

func NewDataProvider() (d *DataProvider, err error) {
	// todo Передать Конфиги!
	d = &DataProvider{}
	d.Database, err = sqlx.Connect("mysql", "root:root@tcp(127.0.0.1:3306)/original?parseTime=true")
	if err != nil {
		return nil, err
	}
	return d, nil
}
