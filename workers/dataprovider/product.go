package dataprovider

import "gitlab.com/logiqone/original-knife-go/model"

func (d *DataProvider) GetNovelties() (*[]model.Product, error) {
	var p []model.Product
	err := d.Database.Select(&p, `
			SELECT
				original.store_product.*,
				original.store_brand.name as brand_name,
				original.store_brand.code as brand_code
			FROM
				original.store_brand,
				original.store_product
			WHERE 
		    	original.store_brand.id = original.store_product.brand_id
			AND 
			    novelty = 1
			ORDER BY 
				sort
		`)
	if err != nil {
		return nil, err
	}
	return &p, nil
}

func (d *DataProvider) GetProductsByCategoryId(id int64) (*[]model.Product, error) {
	var p []model.Product
	err := d.Database.Select(&p, `
		SELECT DISTINCT
			original.store_product.*,
			original.store_brand.name as brand_name,
			original.store_brand.code as brand_code
		FROM
			original.store_brand,
			original.store_product,
			original.store_product_to_category
		WHERE 
		    original.store_product.id = original.store_product_to_category.product_id
		AND 
		    original.store_brand.id = original.store_product.brand_id
		AND
		    original.store_product_to_category.category_id = ?
		ORDER BY sort
		LIMIT 20
		`, id)
	if err != nil {
		return nil, err
	}
	return &p, nil
}

func (d *DataProvider) GetProductsByCategoryIdAndBrandId(categoryId int64, brandId int64) (*[]model.Product, error) {
	var p []model.Product
	err := d.Database.Select(&p, `
		SELECT DISTINCT
			original.store_product.*,
			original.store_brand.name as brand_name,
			original.store_brand.code as brand_code
		FROM
			original.store_brand,
			original.store_product,
			original.store_product_to_category
		WHERE 
		    original.store_product.id = original.store_product_to_category.product_id
		AND 
		    original.store_brand.id = original.store_product.brand_id
		AND
		    original.store_product_to_category.category_id = ?
		AND
		    original.store_brand.id = ?
		ORDER BY sort
		LIMIT 20
		`, categoryId, brandId)
	if err != nil {
		return nil, err
	}
	return &p, nil
}

func (d *DataProvider) GetProductsByBrandCode(brandCode string) (*[]model.Product, error) {
	var p []model.Product
	err := d.Database.Select(&p, `
		SELECT DISTINCT
			original.store_product.*,
			original.store_brand.name as brand_name,
			original.store_brand.code as brand_code
		FROM
			original.store_brand,
			original.store_product
		WHERE 
		    original.store_brand.id = original.store_product.brand_id
		AND
		    original.store_brand.code = ?
		ORDER BY sort
		LIMIT 20
		`, brandCode)
	if err != nil {
		return nil, err
	}
	return &p, nil
}

func (d *DataProvider) GetProductsByCollectionId(id int64) (*[]model.Product, error) {
	var p []model.Product
	err := d.Database.Select(&p, `
		SELECT DISTINCT
			original.store_product.*,
			original.store_brand.name as brand_name,
			original.store_brand.code as brand_code
		FROM
			original.store_brand,
			original.store_product
		WHERE 
		    original.store_brand.id = original.store_product.brand_id
		AND
		    original.store_product.collection_id = ?
		ORDER BY sort
		LIMIT 20
		`, id)
	if err != nil {
		return nil, err
	}
	return &p, nil
}

func (d *DataProvider) GetProductByCode(code string) (*model.Product, error) {
	var p model.Product
	err := d.Database.Get(&p, `
		SELECT
			original.store_product.*,
			original.store_brand.name as brand_name,
			original.store_brand.code as brand_code
		FROM
			original.store_brand,
			original.store_product
		WHERE 
		    original.store_brand.id = original.store_product.brand_id
		AND
		    original.store_product.code = ?
		`, code)
	if err != nil {
		return nil, err
	}
	return &p, nil
}

func (d *DataProvider) GetClassKnifeByProductId(id int64) (*model.ClassKnife, error) {
	var k model.ClassKnife
	err := d.Database.Get(&k, `
		SELECT
			original.store_class_knife.*
		FROM
			original.store_class_knife
		WHERE 
		    original.store_class_knife.product_id = ?
		`, id)
	if err != nil {
		return nil, err
	}
	println("noerr")
	return &k, nil
}

func (d *DataProvider) GetClassTypeByProductId(id int64) (*model.ClassType, error) {
	var t model.ClassType
	err := d.Database.Get(&t, `
		SELECT
			original.store_class_type.*
		FROM
			original.store_class_type
		WHERE 
		    original.store_class_type.product_id = ?
		`, id)
	if err != nil {
		return nil, err
	}
	return &t, nil
}
